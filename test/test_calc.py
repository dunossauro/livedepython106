from unittest import TestCase
from app.calc import soma, sub


class TestCalc(TestCase):
    def test_soma(self):
        self.assertEqual(soma(2, 2), 4)

    def test_sub(self):
        self.assertEqual(sub(2, 2), 0)
