"""Módulo da minha calculadora pessoal."""


def soma(x, y):
    """Soma dois tipos somáveis."""
    return x + y


def sub(x, y):
    """Subtrai dois tipos subtratíveis."""
    return x - y
